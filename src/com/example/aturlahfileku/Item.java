package com.example.aturlahfileku;

public class Item implements Comparable<Item> {
	private String name;
	private String data;
	private String date;
	private String path;
	private String image;

	public Item(String nama, String dat, String tgl, String dir, String img) {
		name = nama;
		data = dat;
		date = tgl;
		path = dir;
		image = img;
	}
	
	public String getName() {
		return name;
	}
	
	public String getData() {
		return data;
	}
	
	public String getDate() {	
		return date;
	}
	
	public String getPath() {
		return path;
	}
	
	public String getImage() {
		return image;
	}
	
	@Override
	public int compareTo(Item i) {
		// TODO Auto-generated method stub
		if(this.name != null) {
			return this.name.toLowerCase().compareTo(i.getName().toLowerCase());
		} else {
			throw new IllegalArgumentException();
		}
	}
}
