package com.example.aturlahfileku;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.sql.Date;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.support.v4.app.DialogFragment;
import android.app.ListActivity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends FragmentActivity {

	private File currDirectory;
	private ListFileAdapter adapter;
	private ListView listView;
	
	private FileFilter filterDirectoriesAndTxt;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.list_view_layout);
		listView = (ListView) findViewById(R.id.listView);
		filterDirectoriesAndTxt = new FileFilter() {
			
			@Override
			public boolean accept(File pathname) {
				if(pathname.isFile()&& pathname.getName().endsWith(".txt"))
					return true;
				else if (pathname.isDirectory())
					return true;
				return false;
			}
		};
		
		currDirectory = null;
		String extState = Environment.getExternalStorageState(); 
		if (!extState.equals(Environment.MEDIA_MOUNTED)) {
			Toast.makeText(this, "External media not mounted!", Toast.LENGTH_LONG).show();
		}else {
			currDirectory = new File(Environment.getExternalStorageDirectory()
					.getPath());
			listFile(currDirectory);
		}
	}
	
	private void listFile(File f) {
		File[]dirs = f.listFiles(filterDirectoriesAndTxt);
		this.setTitle(f.getName());
		
		List<Item> any = new ArrayList<Item>();
		
		List<Item>dir = new ArrayList<Item>();
		List<Item>fls = new ArrayList<Item>();
		try {
			for(File ff: dirs) {
				Date lastDate = new Date(ff.lastModified());
				DateFormat dateFormat = DateFormat.getDateTimeInstance();
				String date_modify = dateFormat.format(lastDate);
				
				if(ff.isDirectory()) {
					File[] fbuf = ff.listFiles();
					int buf = 0;
					if(fbuf != null) {
						buf = fbuf.length;
					} else {
						buf = 0;						
					}
					String num_item = String.valueOf(buf);
					if(buf == 0 || buf == 1) { 
//						num_item = num_item + " item";
						num_item = num_item + " file teks / direktori";
					} else { 
//						num_item = num_item + " items";
						num_item = num_item + " file teks / direktori";
					}
//					dir.add(new Item(ff.getName(),num_item,date_modify,ff.getAbsolutePath(),"folder")); 
					any.add(new Item(ff.getName(),num_item,date_modify,ff.getAbsolutePath(),"folder")); 
				} else {
//					fls.add(new Item(ff.getName(),ff.length() + " Byte", date_modify, ff.getAbsolutePath(),"txt"));
					any.add(new Item(ff.getName(),ff.length() + " Byte", date_modify, ff.getAbsolutePath(),"txt"));
				}
			}
		} catch(Exception e) {
			
		}
		Collections.sort(dir);
		Collections.sort(fls);
		if (!f.getName().equalsIgnoreCase("sdcard")) {
//			dir.add(0, new Item("..", "Parent Directory", "", f.getParent(),"up"));
			any.add(0, new Item("..", "Parent Directory", "", f.getParent(),"up"));
		}
//		 adapter = new ListFileAdapter(MainActivity.this,R.layout.activity_main,dir);
		 adapter = new ListFileAdapter(MainActivity.this,R.layout.activity_main,any);
		 listView.setAdapter(adapter); 
		 listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				Item o = adapter.getItem(arg2);
				if(o.getImage().equalsIgnoreCase("folder")||o.getImage().equalsIgnoreCase("up")){
					currDirectory = new File(o.getPath());
					listFile(currDirectory);
				} else {
					//Source code untuk melakukan aksi pada file yang dipilih
					EditTextInterface textInterface = new EditTextDialogFragment();
					textInterface.setItem(o);
					textInterface.setAdapter(adapter);
					((EditTextDialogFragment)textInterface).show(getSupportFragmentManager(), "EditTextDialog");
				}
			}
		});
	}
	
//	@SuppressLint("NewApi")
//	protected void onListItemClick(ListView l, View v, int position, long id) {
//		// TODO Auto-generated method stub
//		super.onListItemClick(l, v, position, id);
//		Item o = adapter.getItem(position);
//		if(o.getImage().equalsIgnoreCase("folder")||o.getImage().equalsIgnoreCase("up")){
//			currDirectory = new File(o.getPath());
//			listFile(currDirectory);
//		} else {
//			//Source code untuk melakukan aksi pada file yang dipilih
//			EditTextInterface textInterface = new EditTextDialogFragment();
//			textInterface.setItem(o);
//			textInterface.setAdapter(adapter);
//			((EditTextDialogFragment)textInterface).show(getFragmentManager(), "EditTextDialog");
//		}
//	}
	
}

class EditTextDialogFragment extends DialogFragment implements EditTextInterface{
	
	Item item;
	Dialog dialog;
	EditText editText;
	String textToBeCopied;
	ImageButton btnCopy;
	ImageButton btnPaste;
	ImageButton btnSave;
	ImageButton btnClose;
	ImageButton btnDelete;
	
//	ClipboardManager clipboardManager;
	File file;
	private ListFileAdapter adapter;
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		
		//clipboardManager = (ClipboardManager) getActivity().getSystemService(getActivity().getApplicationContext().CLIPBOARD_SERVICE);
		dialog = new Dialog(getActivity());
		file = new File(item.getPath());
		BufferedReader reader=null;
		String contentStr="";
		
		LayoutInflater inflater = LayoutInflater.from(getActivity());
		View dialogView = inflater.inflate(R.layout.dialog_text_editor, null);
		editText = (EditText) dialogView.findViewById(R.id.editText1);
		btnCopy = (ImageButton) dialogView.findViewById(R.id.btnCopy);
		btnPaste = (ImageButton) dialogView.findViewById(R.id.btnPaste);		
		btnSave = (ImageButton) dialogView.findViewById(R.id.btnSave);
		btnClose = (ImageButton) dialogView.findViewById(R.id.btnClose);
		btnDelete = (ImageButton) dialogView.findViewById(R.id.btnDelete);
		
		btnClose.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				if(dialog!=null){
					dialog.dismiss();
				}
			}
		});
		
		btnSave.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				try {
					file.createNewFile();
					FileOutputStream outputStream = new FileOutputStream(file);
					OutputStreamWriter streamWriter = new OutputStreamWriter(outputStream);
					streamWriter.append(editText.getText());
					streamWriter.close();
					Toast.makeText(getActivity(), "File disimpan... :)", Toast.LENGTH_SHORT).show();
				} catch (IOException ex) {
					Toast.makeText(getActivity(), "File gagal disimpan... :(", Toast.LENGTH_SHORT).show();
				}
			}
		});
		
		btnPaste.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				if(editText.isFocused()){
					int start = editText.getSelectionStart();
//					ClipData.Item item = clipboardManager.getPrimaryClip().getItemAt(0);
					if(item==null)return;
					String textToBePasted = getCopiedText();
					
					editText.getEditableText().insert(start, textToBePasted);
					Toast.makeText(getActivity(), "'"+textToBePasted+"' ditempel... :)", Toast.LENGTH_SHORT).show();
				}
			}
		});
		
		btnCopy.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				if(editText.getSelectionEnd()>editText.getSelectionStart()){
					textToBeCopied = editText.getText().subSequence(editText.getSelectionStart(), editText.getSelectionEnd()).toString();
				}else{
					textToBeCopied = editText.getText().subSequence(editText.getSelectionEnd(), editText.getSelectionStart()).toString();
				}
//				ClipData clip = ClipData.newPlainText("selectedText", textToBeCopied);
//				clipboardManager.setPrimaryClip(clip);
				putCopyText(textToBeCopied);
				Toast.makeText(getActivity(), "'"+textToBeCopied+"' disalin... :)", Toast.LENGTH_SHORT).show();
			}
		});
		
		btnDelete.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				final Dialog d = dialog;
				final Item i = item;
				final ListFileAdapter a = adapter;
				AlertDialog.Builder confirm = new AlertDialog.Builder(getActivity());
				confirm.setMessage("Hapus File Ini?")
					.setPositiveButton("OK", new DialogInterface.OnClickListener() {						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							file.delete();
							a.remove(i);
							if(d!=null) {
								d.dismiss();
							}
							Toast.makeText(getActivity(), "Berkas Berhasil Dihapus", Toast.LENGTH_SHORT).show();
						}
					})
					.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							//						
						}
					});
				confirm.create();
				confirm.show();
			}			
		});
		
		dialog.setTitle(item.getName());
		dialog.setContentView(dialogView);
		try {
			reader = new BufferedReader(new FileReader(file));
			String line;
			while ((line = reader.readLine())!=null) {
				contentStr += line+'\n';
			}
			reader.close();
			editText.setText(contentStr);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return dialog;
	}
	
	@Override
	public View getView() {
		// TODO Auto-generated method stub
		return super.getView();
	}

	@Override
	public void setItem(Item item) {
		this.item = item;
	}

	@Override
	public Item getItem() {
		return item;
	}

	@Override
	public void setAdapter(ListFileAdapter adapter) {
		this.adapter = adapter;
	}
	
	@SuppressWarnings("deprecation")
	public void putCopyText(String text){
	    int sdk = android.os.Build.VERSION.SDK_INT;
	    if(sdk < android.os.Build.VERSION_CODES. HONEYCOMB) {
	        android.text.ClipboardManager clipboard = (android.text.ClipboardManager) getActivity().getSystemService(getActivity().getApplicationContext().CLIPBOARD_SERVICE);
	        clipboard.setText(text);
	    } else {
	        android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getActivity().getSystemService(getActivity().getApplicationContext().CLIPBOARD_SERVICE); 
	        android.content.ClipData clip = ClipData.newPlainText("simple text",text);
	        clipboard.setPrimaryClip(clip);
	    }
	}

	@SuppressWarnings("deprecation")
	public String getCopiedText(){
	    String text = null;
	    int sdk = android.os.Build.VERSION.SDK_INT;
	    if(sdk < android.os.Build.VERSION_CODES. HONEYCOMB ) {
	        android.text.ClipboardManager clipboard = (android.text.ClipboardManager) getActivity().getSystemService(getActivity().getApplicationContext().CLIPBOARD_SERVICE);
	        text =  clipboard.getText().toString();
	    } else {
	        android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getActivity().getSystemService(getActivity().getApplicationContext().CLIPBOARD_SERVICE); 
	        text =  clipboard.getText().toString();
	    }
	    return text;
	}
}

interface EditTextInterface {
	public void setItem(Item item);
	public void setAdapter(ListFileAdapter adapter);
	public Item getItem();
}
