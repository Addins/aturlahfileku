package com.example.aturlahfileku;

import java.util.List;

import com.example.aturlahfileku.Item;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ListFileAdapter extends ArrayAdapter<Item>{
	private Context c;
	private int id;
	private List<Item> items;	

	public ListFileAdapter(Context context, int textViewResourceId,
			List<Item> objects) {
		super(context, textViewResourceId, objects);
		// TODO Auto-generated constructor stub
		c = context;
		id = textViewResourceId;
		items = objects;
	}
	
	public Item getItem(int i) {
		 return items.get(i);
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;
		if(v == null) {
			LayoutInflater vi = (LayoutInflater)c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(id, null);
		}
		
		final Item o = items.get(position);
		if(o != null) {
			TextView txt_name = (TextView) v.findViewById(R.id.txt_fd_name);
			TextView txt_data = (TextView) v.findViewById(R.id.txt_fd_size);
			TextView txt_date = (TextView) v.findViewById(R.id.txt_fd_date);			
			ImageView img_icon = (ImageView) v.findViewById(R.id.ic_fd);
			
			String uri = "drawable/"+o.getImage();
			int imageResource = c.getResources().getIdentifier(uri, null, c.getPackageName());
			Drawable image = c.getResources().getDrawable(imageResource);
			img_icon.setImageDrawable(image);
			
			if(txt_name != null) txt_name.setText(o.getName());
			if(txt_data != null) txt_data.setText(o.getData());
			if(txt_date != null) txt_date.setText(o.getDate());						
		}
		return v;
		
	}
	
}
